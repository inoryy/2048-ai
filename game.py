from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time, re

class Game:
    browser = None
    moves = {'l':Keys.ARROW_LEFT,'d':Keys.ARROW_DOWN,'u': Keys.ARROW_UP, 'r':Keys.ARROW_RIGHT}

    def get_browser(self):
        if not self.browser:
            browser = webdriver.Firefox()
            browser.get('http://gabrielecirulli.github.io/2048/')
            browser.maximize_window()
            time.sleep(2)
            browser.execute_script("""document.querySelector(".app-notice").remove();""")
            browser.execute_script("window.scrollBy(0,100)")
            self.browser = browser
            
        return self.browser

    def start_game(self):
        browser = self.get_browser()
        browser.find_element_by_class_name("restart-button").click()
        
    def is_finished(self):
        return len(self.browser.find_elements_by_class_name("game-over")) == 1
    
    def end_game(self):
        print("Game finished, score: %d" % self.get_score())
        print("Closing in 3...")
        time.sleep(3)
        self.browser.close()
        
    def get_score(self):
        if not self.browser:
            return 0
        score = self.browser.find_element_by_class_name("score-container").text
        return int(score.split("\n", 1)[0])
        
    def get_tiles(self):
        tiles = [[0,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,0]]

        for t in self.browser.find_elements_by_class_name("tile"):
            nums = re.findall(r"\d+", str(t.get_attribute("class")))
            tiles[int(nums[2]) - 1][int(nums[1]) - 1] = int(nums[0])
            
        return tiles
        
    def move(self, move):
        key = self.moves[move]
        self.browser.find_element_by_tag_name("body").send_keys(key)
        
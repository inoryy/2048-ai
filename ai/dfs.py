from ai.base import BaseAI

class DfsAI(BaseAI):
    def next_move(self, tiles, d=3):
        #print("Calculating move, depth: " + str(d))
        move_scores = {'u':0,'d':0,'l':0,'r':0}
        best_move = 'u'
        for move in ['u', 'd', 'l', 'r']:
            tcp = [r[:] for r in tiles]
#             m = move
#             for _ in range(d):
            tcp = self.emulate_move(tcp, move)
#                 m = self.next_move(tcp,d-1)
            move_scores[move] = self.get_tiles_score(tcp)
            if (move_scores[move] > move_scores[best_move]):
                best_move = move
        return best_move
        
    def get_tiles_score(self, tiles):
        score = 0
        for i in range(4):
            for j in range(4):
                score += tiles[i][j]
        return score
from collections import deque
from ai.base import BaseAI

class CyclicAI(BaseAI):
    moves = deque(['u', 'r', 'u', 'l'])
    
    def next_move(self, tiles):
        for _ in range(3):
            move = self.moves.popleft()
            self.moves.append(move)
        #    if self.can_move(tiles, move):
        return move
        #return 'd'
class NaiveAI():
    moves = ['u']
    prev_tiles = None
    
    def next_move(self, tiles):
        max_adj = 1
        move = 'u'
        for i in range(3):
            for j in range(3):
                if (tiles[i][j] == tiles[i+1][j] and tiles[i][j] >= max_adj):
                    max_adj = tiles[i][j]
                    move = 'd'
                if (tiles[i][j] == tiles[i][j+1] and tiles[i][j] >= max_adj):
                    max_adj = tiles[i][j]
                    move = 'l'
                
        if max_adj == 1:
            if self.prev_tiles == tiles and self.moves[-1] != 'r':
                move = 'r'
            elif self.moves[-1] == 'u':
                move = 'd'
            else:
                move = 'u'
        self.moves.append(move)
        self.prev_tiles = tiles
                
        return move
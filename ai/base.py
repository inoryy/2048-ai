class BaseAI():
    def can_move(self, tiles, move):
        for i in range(4):
            for j in range(4):
                if tiles[i][j] > 0:
                    if move == 'u':
                        if i > 0 and (tiles[i-1][j] == 0 or tiles[i-1][j] == tiles[i][j]):
                            return True
                    if move == 'd':
                        if i < 3 and (tiles[i+1][j] == 0 or tiles[i+1][j] == tiles[i][j]):
                            return True
                    if move == 'l':
                        if j > 0 and (tiles[i][j-1] == 0 or tiles[i][j-1] == tiles[i][j]):
                            return True
                    if move == 'r':
                        if j < 3 and (tiles[i][j+1] == 0 or tiles[i][j+1] == tiles[i][j]):
                            return True
        return False
    
    # loosely based on https://github.com/ayberkt/2048/blob/master/game.py
    # @todo extract this somewhere and test
    def emulate_move(self, tiles, move):
        from random import randint, choice

        def get_col(n):
            column = []
            for row in tiles:
                column.append(row[n])
            return column
    
        def set_col(n, col):
            for i in range(4):
                tiles[i][n] = col[i]
                
        def slide(direction):
            if direction == "u":
                for i in range(4):
                    column = get_col(i)
                    column = shift(column, "l")
                    set_col(i, column)
            elif direction == "r":
                for i in range(4):
                    row = tiles[i]
                    row = shift(row, "r")
                    tiles[i] = row
            elif direction == "d":
                for i in range(4):
                    column = get_col(i)
                    column = shift(column, "r")
                    set_col(i, column)
            elif direction == "l":
                for i in range(4):
                    row = tiles[i]
                    row = shift(row, "l")
                    tiles[i] = row
            insert_random_num()
            return tiles
    
        def shift(array, direction):
            if sum(array) == 0: return array
            if direction == 'r': array = array[::-1]
    
            array = list(filter(lambda x: x != 0, array))
    
            for index in range(1, len(array)):
                if array[index - 1] == array[index]:
                    array[index - 1] += array[index]
                    array[index] = 0
            array = list(filter(lambda x: x != 0, array))
    
            while len(array) < 4:
                array.append(0)
    
            if direction == 'l': return array
            
            if direction == 'r': return array[::-1]
            
        def insert_random_num():
            x = randint(0, 3)
            y = randint(0, 3)
    
            while not tiles[y][x] == 0:
                x = randint(0, 3)
                y = randint(0, 3)
    
            # 90% of 2, 10% of 4
            choices = ([2]*9+[4])
            tiles[y][x] = choice(choices)

        return slide(move)
from ai.base import BaseAI
from math import log

class HeuristicAI(BaseAI):
    def next_move(self, tiles):
        best_move = 'u'
        best_score = 0
        for move in ['u', 'd', 'l', 'r']:
            if self.can_move(tiles, move):
                tcp = [r[:] for r in tiles]
                self.emulate_move(tcp, move)
                score = self.h_score(tcp, move)
                if score > best_score:
                    best_move = move
                    best_score = score
        #print("Move chosen %s, h_score: %s" % (best_move, best_score))
        return best_move
        
    def h_score(self, tiles, move):
        score = 128
        
        for i in range(4):
            for j in range(4):
                score += tiles[i][j]
                if tiles[i][j] == 0:
                    score += 128
                else:
                    if i < 3:
                        if tiles[i][j] == tiles[i+1][j]:
                            score += tiles[i][j]*2
                        elif tiles[i+1][j] == 0:
                            score += int(1/tiles[i][j] * 4096)
                    if j < 3:
                        if tiles[i][j] == tiles[i][j+1]:
                            score += tiles[i][j]
                        elif tiles[i][j+1] == 0:
                            score += int(1/tiles[i][j] * 4096)
                            
        if tiles[0][0] >= tiles[1][0] and tiles[1][0] >= tiles[2][0] and tiles[2][0] > tiles[3][0]:
            score += 256

        for m in ['u', 'd', 'l', 'r']:
            if self.can_move(tiles, m):
                score += 256 
                
        if move in ['u', 'l']:
            score += 256

        return score
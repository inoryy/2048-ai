# 2048 AI Solver

## About

Please see [this presentation](https://bitbucket.org/inoryy/2048-ai/downloads/2048-AI.pdf).

## Installation

* `sudo apt-get install python3-pip`
* `sudo python3 -m pip install selenium`
* `sudo apt-get install firefox`

## Running

`python3 main.py`

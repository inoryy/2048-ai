import csv, subprocess
from game import Game
from ai.naive import NaiveAI
from ai.cyclic import CyclicAI
from ai.dfs import DfsAI
from ai.heuristic import HeuristicAI

def log_score(ai_name, score):
    with open('score.csv', 'a', encoding='utf8') as f:
        ver_hash = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).strip().decode('utf8')
        w = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        w.writerow([ai_name] + [ver_hash] + [score])

#ai = NaiveAI()
#ai = CyclicAI()
#ai = DfsAI()
ai = HeuristicAI()
game = Game()

game.start_game()
 
while not game.is_finished():
    move = ai.next_move(game.get_tiles())
    game.move(move)
     
log_score(ai.__class__.__name__, game.get_score())
     
game.end_game()
import unittest
from ai.base import BaseAI

class BaseAITest(unittest.TestCase):
    def test_can_move(self):
        ai = BaseAI()
        # moving up blocked/non-blocked
        tiles = [[2,2,2,0], [0,0,0,2], [0,0,0,0], [0,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'u'), True)
        tiles = [[2,2,2,0], [0,0,0,0], [0,0,0,2], [0,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'u'), True)
        tiles = [[2,2,2,2], [0,0,0,0], [0,0,0,0], [0,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'u'), False)
        
        # moving up can merge/can't merge
        tiles = [[2,2,2,2], [4,4,4,2], [0,0,0,0], [0,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'u'), True)
        tiles = [[2,2,2,2], [4,4,4,4], [0,0,0,0], [0,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'u'), False)
        
        # moving left blocked/non-blocked
        tiles = [[2,0,0,0], [2,0,0,0], [0,2,0,0], [2,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'l'), True)
        tiles = [[2,0,0,0], [2,0,0,0], [2,0,0,0], [2,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'l'), False)
        
        # can merge left blocked/non-blocked
        tiles = [[4,0,0,0], [2,2,0,0], [4,0,0,0], [4,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'l'), True)
        tiles = [[4,0,0,0], [4,2,0,0], [4,0,0,0], [2,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'l'), False)
        
        tiles = [[8,4,2,0], [4,2,0,0], [2,0,0,0], [0,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'u'), False)
        self.assertEqual(ai.can_move(tiles, 'l'), False)
        self.assertEqual(ai.can_move(tiles, 'd'), True)
        self.assertEqual(ai.can_move(tiles, 'r'), True)
        
        tiles = [[32,16,8,16], [16,8,2,4], [4,0,0,0], [0,0,0,0]]
        self.assertEqual(ai.can_move(tiles, 'u'), False)
        self.assertEqual(ai.can_move(tiles, 'l'), False)
        self.assertEqual(ai.can_move(tiles, 'd'), True)
        self.assertEqual(ai.can_move(tiles, 'r'), True)
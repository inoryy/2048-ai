import unittest
from ai.dfs import DfsAI

class DfsAITest(unittest.TestCase):
    def test_can_move(self):
        ai = DfsAI()
        tiles = [[2,2,2,0,0], [0,0,0,2,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        expected = [[2,2,2,2,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        self.assertEqual(ai.emulate_move(tiles, 'u'), expected)
        
        tiles = [[2,2,2,2,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        expected = [[2,2,2,2,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        self.assertEqual(ai.emulate_move(tiles, 'u'), expected)
        
        tiles = [[2,2,2,2,0], [4,4,4,2,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        expected = [[2,2,2,4,0], [4,4,4,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        self.assertEqual(ai.emulate_move(tiles, 'u'), expected)
        
        tiles = [[2,2,2,2,0], [4,4,4,4,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        expected = [[2,2,2,2,0], [4,4,4,4,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]
        self.assertEqual(ai.emulate_move(tiles, 'u'), expected)